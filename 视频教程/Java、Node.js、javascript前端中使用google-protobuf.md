### Java、Node.js、javascript前端中使用google-protobuf



#### 1、下载protobuf的编译器

[目前最新版本为Protocol Buffers v3.5.1](https://github.com/google/protobuf/releases)



#### 2、配置环境变量

解压 `protoc-3.5.1-osx-x86_64.zip`

Mac 配置环境变量 `vi ~/.bash_profile` 使其配置生效`source ~/.bash_profile`

```json
#protobuf
export PROTOBUF_HOME=/Users/Javen/Documents/dev/java/protobuf/protoc-3.5.1-osx-x86_64
export PATH=$PATH:$PROTOBUF_HOME/bin
```

Window  将bin添加到path 即可 例如:`D:\protobuf\protoc-3.5.1-win32\bin`



`本文在Mac环境下编写`  **Mac**与**window**命令唯一的区别就是需要将`protoc`改成`protoc.exe` 前提是需要添加环境变量。



#### 3、编写一个proto文件

文件保存为`chat.proto` 此`proto`文件摘自[**t-io 让天下没有难开发的网络编程**](https://gitee.com/tywo45/t-io)

```protobuf
syntax = "proto3";
package com.im.common.packets;

option java_package = "com.im.common.packets";  //设置java对应的package
option java_multiple_files = true; //建议设置为true，这样会每个对象放在一个文件中，否则所有对象都在一个java文件中

/**
 * 聊天类型
 */
enum ChatType {
	CHAT_TYPE_UNKNOW = 0;//未知
	CHAT_TYPE_PUBLIC = 1;//公聊
	CHAT_TYPE_PRIVATE = 2;//私聊
}
/**
 * 聊天请求
 */
message ChatReqBody {
	int64 time = 1;//消息发送时间
	ChatType type = 2; //聊天类型
	string text = 3; //聊天内容
	string group = 4; //目标组id
	int32 toId = 5; //目标用户id，
	string toNick = 6; //目标用户nick
}

/**
 * 聊天响应
 */
message ChatRespBody {
	int64 time = 1;//消息发送时间
	ChatType type = 2; //聊天类型
	string text = 3; //聊天内容
	int32 fromId = 4; //发送聊天消息的用户id
	string fromNick = 5; //发送聊天消息的用户nick
	int32 toId = 6; //目标用户id
	string toNick = 7; //目标用户nick
	string group = 8; //目标组id
}
```





#### 4、编译器对其进行编译

##### 4.1 编译为Java

进入到项目的包目录`cd com/im/common/packets`执行以下编译命名

```protobuf
protoc  --java_out=./ com/im/common/packets/chat.proto
```



##### 4.2 编译为JS

```protobuf
protoc --js_out=import_style=commonjs,binary:. chat.proto
```

执行后会在当前文件夹中生成`chat_pb.js` 文件，这里面就是`protobuf`的API和一些函数。如果是`Node.js` 就可以直接使用了，如果想在浏览器(前端)中使用`protobuf`还需要做一些处理。



#### 5、前端使用protobuf处理步骤

#####  5.1 npm安装需要的库

在`chat_pb.js`文件的同级目录下安装引用库

```json
npm install -g require
npm install google-protobuf
npm install -g browserify
```

##### 5.2 使用browserify对文件进行编译打包

编写脚本**保存为exports.js**

```javascript
var chatProto = require('./chat_pb');  
module.exports = {  
DataProto: chatProto  
}
```

**执行命令** `browserify exports.js > chat.js `对`chat_pb.js`文件进行编译打包生成`chat.js`后就可以愉快的使用了。



#### 6、示例

> 前端中使用

```html
<script src="./chat.js"></script>
<script type="text/javascript">
    var chatReqBody = new proto.com.im.common.packets.ChatReqBody();
    chatReqBody.setTime(new Date().getTime());
    chatReqBody.setText("测试");
    chatReqBody.setType(1);
    chatReqBody.setGroup("Javen");
    chatReqBody.setToid(666);
    chatReqBody.setTonick("Javen205");

    var bytes = chatReqBody.serializeBinary();  
    console.log("序列化为字节:"+bytes);
    var data = proto.com.im.common.packets.ChatReqBody.deserializeBinary(bytes); 
    console.log("反序列化为对象:"+data);  
    console.log("从对象中获取指定属性:"+data.getTonick());
    console.log("对象转化为JSON:"+JSON.stringify(data));  

</script>
```

> Java中使用

java中要用protobuf,protobuf与json相互转换，首先需要引入相关的jar，maven的pom坐标如下

```xml
<dependency>
    <groupId>com.google.protobuf</groupId>
    <artifactId>protobuf-java</artifactId>
    <version>3.5.1</version>
</dependency>
<dependency>
    <groupId>com.googlecode.protobuf-java-format</groupId>
    <artifactId>protobuf-java-format</artifactId>
    <version>1.4</version>
</dependency>
```



```java
public static void test() {
		try {
			JsonFormat jsonFormat = new JsonFormat();
			ChatRespBody.Builder builder = ChatRespBody.newBuilder();
			builder.setType(ChatType.CHAT_TYPE_PUBLIC);
			builder.setText("Javen 测试");
			builder.setFromId(1);
			builder.setFromNick("Javen");
			builder.setToId(110);
			builder.setToNick("Javen.zhou");
			builder.setGroup("Javen");
			builder.setTime(SystemTimer.currentTimeMillis());
			ChatRespBody chatRespBody = builder.build();
			//从protobuf转json
			String asJson = jsonFormat.printToString(chatRespBody);
			System.out.println("Object to json "+asJson);
			
			byte[] bodybyte = chatRespBody.toByteArray();
			//解码是从byte[]转换为java对象
			ChatRespBody parseChatRespBody = ChatRespBody.parseFrom(bodybyte);
			asJson = jsonFormat.printToString(parseChatRespBody);
			System.out.println("bodybyte to json "+asJson);
			
			//从json转protobuf
			ChatRespBody.Builder _builder = ChatRespBody.newBuilder();
			jsonFormat.merge(new ByteArrayInputStream(asJson.getBytes()), _builder);
			ChatRespBody _chatRespBody = _builder.build();
			asJson = jsonFormat.printToString(_chatRespBody);
			System.out.println("json to protobuf "+asJson);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
```

